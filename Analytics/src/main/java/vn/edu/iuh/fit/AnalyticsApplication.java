package vn.edu.iuh.fit;

import net.datafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import vn.edu.iuh.fit.models.Analytics;
import vn.edu.iuh.fit.repositories.AnalyticsRepository;

import java.time.LocalDate;
import java.util.Locale;
import java.util.Random;

@SpringBootApplication
@EnableRetry
public class AnalyticsApplication {

    @Autowired
    private AnalyticsRepository analyticsRepository;

    public static void main(String[] args) {
        SpringApplication.run(AnalyticsApplication.class, args);
    }
    @Bean
    CommandLineRunner InitTransactions(){
        return args -> {
            Faker faker = new Faker(new Locale("vi","VN"));
            for (int i=1; i<=10; i++) {
                Analytics analytics = new Analytics(faker.name().name());
                analyticsRepository.save(analytics);
            }
        };
    }
}