package vn.edu.iuh.fit.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class CircuitBreakerConfiguration {

    @Bean
    public CircuitBreakerRegistry circuitBreakerRegistry() {
        CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
                .failureRateThreshold(1) // Ngưỡng tỷ lệ lỗi
                .slidingWindowSize(10) // Kích thước cửa sổ trượt
                .waitDurationInOpenState(Duration.ofSeconds(30)) // Thời gian mạch ở trạng thái mở
                .build();
        return CircuitBreakerRegistry.of(circuitBreakerConfig);
    }

}
