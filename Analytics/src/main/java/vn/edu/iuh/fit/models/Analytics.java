package vn.edu.iuh.fit.models;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Entity
@Data
@Table(name="Analytic")
@NoArgsConstructor
@AllArgsConstructor
public class Analytics {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "analytic_id")
    private long id;
    private String name;
    @OneToOne
    private Notification notification;

    public Analytics(String name) {
        this.name = name;
    }
}
