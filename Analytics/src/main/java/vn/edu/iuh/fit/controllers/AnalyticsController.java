package vn.edu.iuh.fit.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.Analytics;
import vn.edu.iuh.fit.models.Notification;
import vn.edu.iuh.fit.repositories.AnalyticsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/Analytics")
@Slf4j
public class AnalyticsController {

    @Autowired
    private AnalyticsRepository analyticsRepository;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    RestTemplate restTemplate = new RestTemplate();

    private int retryCount = 0;

    @GetMapping("analytics")
    @Retryable(maxAttempts = 5, backoff = @Backoff(delay = 1000))
    public List<Analytics> getAnalytics() {
        retryCount++;
        return retrieveAnalytics();
    }

    private List<Analytics> retrieveAnalytics() {
        log.info("Retry attempt: {}", retryCount);
        List<Analytics> analyticsList = new ArrayList<>();
        for (long i = 1; i <= 10; i++) {
            Notification notification = restTemplate.getForObject("http://localhost:8085/Notification/notifications/" + i, Notification.class);
            Analytics analytics = analyticsRepository.findById(i).orElse(null);
            if (analytics != null) {
                analytics.setNotification(notification);
                analyticsList.add(analytics);
            }
        }
        return analyticsList;
    }




    @GetMapping("analytics/{id}")
    public List<Analytics> getAnalytic(@PathVariable long id) {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.create("getAnalytic");
        return circuitBreaker.run(() -> {
            Notification notification = restTemplate.getForObject("http://localhost:8085/Notification/notifications/" + id, Notification.class);
            Analytics analytics = analyticsRepository.findById(id).orElse(null);
            if (analytics != null) {
                analytics.setNotification(notification);
            }
            List<Analytics> result = new ArrayList<>();
            if (analytics != null) {
                result.add(analytics);
            }
            return result;
        }, throwable -> {
            log.error("Circuit breaker caught an error: {}", throwable.getMessage());
            return new ArrayList<>(); // Trả về danh sách rỗng khi xảy ra lỗi
        });
    }
}
