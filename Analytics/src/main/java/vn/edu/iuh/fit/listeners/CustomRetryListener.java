package vn.edu.iuh.fit.listeners;//package vn.edu.iuh.fit.listeners;
//
//import org.springframework.retry.RetryCallback;
//import org.springframework.retry.RetryContext;
//import org.springframework.retry.RetryListener;
//
//import java.util.logging.Logger;
//
//public class CustomRetryListener implements RetryListener {
//    private static final Logger logger = Logger.getLogger(CustomRetryListener.class.getName());
//
//    @Override
//    public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
//        return true; // Always allow retry
//    }
//
//    @Override
//    public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
//        // No-op
//    }
//
//    @Override
//    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
//        int retryCount = context.getRetryCount();
//        logger.warning("Retry attempt " + retryCount + " due to: " + throwable.getMessage());
//    }
//}
