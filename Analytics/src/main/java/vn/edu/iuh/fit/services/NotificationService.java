package vn.edu.iuh.fit.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.Notification;
import vn.edu.iuh.fit.repositories.NotificationRepository;

@Service
@Retryable
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;

    RestTemplate restTemplate = new RestTemplate();

    public Notification getNotification(long id){
        return restTemplate.getForObject("http://localhost:8085/Notification/notifications/" + id, Notification.class);

    }
}
