package vn.edu.iuh.fit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.edu.iuh.fit.models.Analytics;

public interface AnalyticsRepository extends JpaRepository<Analytics,Long> {
}
