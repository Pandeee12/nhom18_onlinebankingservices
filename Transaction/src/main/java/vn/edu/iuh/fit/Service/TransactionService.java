package vn.edu.iuh.fit.Service;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.Transaction;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.TransactionRepository;
import vn.edu.iuh.fit.repositories.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;
    @Retryable(value = {IllegalArgumentException.class}, maxAttempts = 5)
    @Transactional
    public void transfer(String fromUsername, String toUsername, long amount) throws InterruptedException {
        User fromUser = userService.getUserByUsername(fromUsername);
        User toUser =  userService.getUserByUsername(toUsername);
        if (fromUser.getBalance().compareTo(amount) >= 0) {
            long fromUserNewBalance = fromUser.getBalance() - amount;
            long toUserNewBalance = toUser.getBalance() + amount;

            userService.updateUserBalanceRemote(fromUsername, fromUserNewBalance);
            userService.updateUserBalanceRemote(toUsername, toUserNewBalance);

            // Lưu thông tin giao dịch thành công
            Transaction transaction = new Transaction(fromUsername, toUsername, amount, LocalDateTime.now(), "Tranfer");
            transactionRepository.save(transaction);
        } else {

            throw new IllegalArgumentException("Số dư tài khoản không đủ");
        }
    }
    @Retryable(value = {IllegalArgumentException.class}, maxAttempts = 5)
    @Transactional
    public void deposit(String Username , long amount) throws InterruptedException {
        User user = userService.getUserByUsername(Username);

        if (amount >= 0) {
            long NewBalance = user.getBalance() + amount;

            userService.updateUserBalanceRemote(user.getUsername(), NewBalance);


            // Lưu thông tin giao dịch thành công
            Transaction transaction = new Transaction("System", user.getUsername(), amount, LocalDateTime.now(), "Deposit");
            transactionRepository.save(transaction);
        } else {
            throw new IllegalArgumentException("Số tiền nạp không hợp lệ");
        }
    }
}
