package vn.edu.iuh.fit.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Data
@Table(name="Transaction")
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fromUsername;
    private String toUsername;
    private Long amount;
    private LocalDateTime timestamp;
    private String status; // success, failed, pending, etc.

    public Transaction(String fromUsername, String toUsername, Long amount, LocalDateTime timestamp, String status) {
        this.fromUsername = fromUsername;
        this.toUsername = toUsername;
        this.amount = amount;
        this.timestamp = timestamp;
        this.status = status;
    }
}
