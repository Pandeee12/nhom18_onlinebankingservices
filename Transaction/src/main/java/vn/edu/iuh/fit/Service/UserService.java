package vn.edu.iuh.fit.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.UserRepository;

import java.math.BigDecimal;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    private static final String USER_SERVICE_URL = "http://localhost:8081/User";

    public User updateUserBalance(String username, long amount) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new IllegalArgumentException("User không tồn tại: " + username));
        user.setBalance(amount);
        userRepository.save(user);
        return user;
    }

    public User getUserByUsername(String username) {
        return restTemplate.getForObject(USER_SERVICE_URL+"/users/"+username,User.class);
    }

    public void updateUserBalanceRemote(String username, long balance) {
        String url = USER_SERVICE_URL + "/updateBalance/" + username + "?balance=" + balance;
        restTemplate.put(url, null);
    }
}
