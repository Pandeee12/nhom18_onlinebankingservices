package vn.edu.iuh.fit.controllers;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.Service.TransactionService;
import vn.edu.iuh.fit.models.Transaction;

import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.TransactionRepository;
import vn.edu.iuh.fit.repositories.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("Transaction")
public class TransactionController {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    private RestTemplate restTemplate;

    private static final String SERVICE_TRANSACTION = "serviceTransaction";


    @RateLimiter(name="myRateLimiter")
    @GetMapping("transactions")
    public List<Transaction> getTransactions(){
        return transactionRepository.findAll();
    }
    @GetMapping("users")
    public List<User> getUsers(){
        return userRepository.findAll();
    }



    @GetMapping("transactions/{id}")
    public Optional<Transaction> getTransaction(@PathVariable long id){
        return transactionRepository.findById(id);
    }
    @RateLimiter(name="myRateLimiter")
    @PostMapping("/transfer")
    //@CircuitBreaker(name = SERVICE_TRANSACTION, fallbackMethod = "serviceTransactionFallback")
    public String transfer(@RequestBody Map<String, Object> transferData) {
        String fromUsername = (String) transferData.get("fromUsername");
        String toUsername = (String) transferData.get("toUsername");
        Integer amount = (Integer) transferData.get("amount");
        long amountL = amount.longValue();
        try {
            transactionService.transfer(fromUsername, toUsername, amountL);
            return "Giao dịch thành công";
        } catch (Exception e) {
            return "Giao dịch thất bại: " + e.getMessage();
        }
    }
    @RateLimiter(name="myRateLimiter")
    @PostMapping("/deposit")
    //@CircuitBreaker(name = SERVICE_TRANSACTION, fallbackMethod = "serviceTransactionFallback")
    public String deposit(@RequestBody Map<String, Object> transferData) {
        String toUsername = (String) transferData.get("toUsername");
        Integer amount = (Integer) transferData.get("amount");
        long amountL = amount.longValue();
        try {
            transactionService.deposit(toUsername, amountL);
            return "Giao dịch thành công";
        } catch (Exception e) {
            return "Giao dịch thất bại: " + e.getMessage();
        }
    }

//    public ResponseEntity<List<User>> serviceTransactionFallback(final Throwable throwable){
//        return new ResponseEntity<>(Collections.emptyList(), HttpStatus.SERVICE_UNAVAILABLE);
//    }
}