package vn.edu.iuh.fit.repositories;

import vn.edu.iuh.fit.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
