package vn.edu.iuh.fit.controllers;

import org.springframework.data.redis.core.RedisTemplate;
import vn.edu.iuh.fit.authen.UserPrincipal;
import vn.edu.iuh.fit.models.Token;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.services.TokenService;
import vn.edu.iuh.fit.services.UserService;
import vn.edu.iuh.fit.Util.JwtUtil;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody User user){
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        User createdUser = userService.createUser(user);
//        userService.createAccount(user);
        return ResponseEntity.ok(createdUser);
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user, HttpSession session){

        UserPrincipal userPrincipal =
                userService.findByUsername(user.getUsername());

        if (null == user || !new BCryptPasswordEncoder()
                .matches(user.getPassword(), userPrincipal.getPassword())) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Account or password is not valid!");
        }

        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));

        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);

        session.setAttribute("token", token.getToken());
        redisTemplate.opsForValue().set("Token", token.getToken());
        return ResponseEntity.ok(token.getToken());
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout() {
        userService.deleteAllUsersFromRedis();
        return ResponseEntity.ok("All users deleted from Redis.");
    }
    @GetMapping("/hello")
//    @PreAuthorize("hasAnyAuthority('USER_READ')")
    public ResponseEntity hello(){
        return ResponseEntity.ok("hello");
    }


//    Object principal = SecurityContextHolder
//            .getContext().getAuthentication().getPrincipal();
//
//        if (principal instanceof UserDetails) {
//        UserPrincipal userPrincipal = (UserPrincipal) principal;
//    }

}
