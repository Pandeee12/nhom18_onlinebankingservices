package vn.edu.iuh.fit.services;

import vn.edu.iuh.fit.models.Token;

public interface TokenService {
    Token createToken(Token token);

    Token findByToken(String token);

}
