package vn.edu.iuh.fit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.User;

@SpringBootApplication
@EnableJpaAuditing
@EnableDiscoveryClient
public class AccountApplication {

	@Autowired

	public static void main(String[] args) {
		SpringApplication.run(AccountApplication.class, args);
	}
	@Bean
	LettuceConnectionFactory jedLettuceConnectionFactory(){
		return new LettuceConnectionFactory();
	}
	@Bean
	RedisTemplate redisTemplate(){
		RedisTemplate redisTemplate = new RedisTemplate();
		redisTemplate.setConnectionFactory(jedLettuceConnectionFactory());
		return redisTemplate;
	}

	@Bean
	public RestTemplate RestTemplate() {
		return new RestTemplate();
	}
}
