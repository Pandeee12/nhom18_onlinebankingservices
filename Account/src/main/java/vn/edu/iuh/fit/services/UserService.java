package vn.edu.iuh.fit.services;

import vn.edu.iuh.fit.authen.UserPrincipal;
import vn.edu.iuh.fit.models.User;

public interface UserService {
    User createUser(User user);
    void deleteAllUsersFromRedis();
    void saveUserToRedis(User user);
    UserPrincipal findByUsername(String username);
}
