package vn.edu.iuh.fit.repositories;

import vn.edu.iuh.fit.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token);
}
