package vn.edu.iuh.fit.services;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.authen.UserPrincipal;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RestTemplate restTemplate;

    private static final String USER_SERVICE_URL = "http://localhost:8081/User";

    @Override
    public User createUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public void deleteAllUsersFromRedis() {
        redisTemplate.getConnectionFactory().getConnection().flushAll();
    }


    @Override
    public void saveUserToRedis(User user) {
        redisTemplate.opsForValue().set("Username:" + user.getUsername() + " " + "Password:" + user.getPassword(), user);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();

        if (null != user) {

            Set<String> authorities = new HashSet<>();

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);

        }

        return userPrincipal;

    }


}