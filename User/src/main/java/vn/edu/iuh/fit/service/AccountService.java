package vn.edu.iuh.fit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.AccountRepository;

@Service


public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public boolean updateBalance(String username, Long balance) {
        User user = accountRepository.findByUsername(username);
        if (user!=null) {
            user.setBalance(balance);
            accountRepository.saveAndFlush(user);
            return true;
        } else {
            return false;
        }
    }
    public User createAccount(User user) {
        return accountRepository.saveAndFlush(user);
    }
}
