package vn.edu.iuh.fit;

import net.datafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import vn.edu.iuh.fit.repositories.AccountRepository;
import java.util.Locale;
import java.util.Random;

@SpringBootApplication
@EnableRetry
public class UserApplication {

	@Autowired
	private AccountRepository accountRepository;

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}

//	@Bean
//	CommandLineRunner test(){
//		return args -> {
//			for (int i=1; i<=10; i++) {
//				Account user = new Account("Account "+i,50000L);
//				accountRepository.save(user);
//			}
//		};
//	}
}
