package vn.edu.iuh.fit.models;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Data
@Table(name="Users")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private long id;
    @Column(name="user_name")
    private String username;
    @Column(name="password")
    private String password;
    @Column(name="balance")
    private long balance;

    public User(String username, long balance) {
        this.username = username;
        this.balance = balance;
    }
    public User(String username,String password, long balance) {
        this.username = username;
        this.password=password;
        this.balance = balance;
    }
}
