package vn.edu.iuh.fit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.edu.iuh.fit.models.User;
import vn.edu.iuh.fit.repositories.AccountRepository;
import vn.edu.iuh.fit.service.AccountService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/User")
public class UserController {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountService accountService;

    @GetMapping("/users")
    public List<User> getUsers(){
        return accountRepository.findAll();
    }

    @GetMapping("/users/{username}")
    public User getUserByName(@PathVariable String username){

        return accountRepository.findByUsername(username);
    }
    @PutMapping("/updateBalance/{username}")
    public ResponseEntity<String> updateBalance(@PathVariable String username, @RequestParam Long balance) {

        boolean isUpdated = accountService.updateBalance(username, balance);
        if (isUpdated) {
            return ResponseEntity.ok("Balance updated successfully."+accountRepository.findByUsername(username));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
        }
    }
//    @PostMapping("/create")
//    @ResponseStatus(HttpStatus.CREATED)
//    public ResponseEntity<String> createUser(@RequestBody User user) {
//        accountService.createAccount(user);
//        return  ResponseEntity.ok(""+user);
//    }
}
