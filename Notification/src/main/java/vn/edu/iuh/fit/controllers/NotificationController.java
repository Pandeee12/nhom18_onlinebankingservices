package vn.edu.iuh.fit.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.models.Account;
import vn.edu.iuh.fit.models.Notification;
import vn.edu.iuh.fit.repositories.NotificationRepository;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/NotificationController")
public class NotificationController {

    @Autowired
    private NotificationRepository notificationRepository;

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping("notifications")
    public List<Notification> getNotifications(){
        return notificationRepository.findAll();
    }

    @GetMapping("notifications/{id}")
    Notification getNotification(@PathVariable long id,@RequestBody Account name){
        Account account = restTemplate.getForObject("http://localhost:8081/User/users/"+name,Account.class);
        Notification notification = notificationRepository.findById(id).get();
        return notification;
    }
}

