package vn.edu.iuh.fit;

import net.datafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import vn.edu.iuh.fit.models.Notification;
import vn.edu.iuh.fit.repositories.NotificationRepository;

import java.util.Locale;

@SpringBootApplication
public class NotificationApplication {

	@Autowired
	private NotificationRepository notificationRepository;

	public static void main(String[] args) {
		SpringApplication.run(NotificationApplication.class, args);
	}

	@Bean
	CommandLineRunner InitTransactions(){
		return args -> {
			Faker faker = new Faker(new Locale("vi","VN"));
			for (int i=1; i<=10; i++) {
				Notification notification = new Notification(faker.name().title());
				notificationRepository.save(notification);
			}
		};
	}
}

