package vn.edu.iuh.fit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.edu.iuh.fit.models.Notification;

public interface NotificationRepository extends JpaRepository<Notification,Long> {
}
